# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
This repo contains all code and resources required and used in INFO3406 Stage 3.
* Version
1.00
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
1. Download SourceTree on MAC/PC
2. Register / log in to your bitbucket account
3. Clone the repo to your pc
4. Start Working and push code :)
* Requirement :
1. R Studio
2. RapidMiner Studio

### Contribution guidelines ###
* Code review
- Make sure all the new function is compatible with the old function before pushing the code
* Other guidelines

### Who do I talk to? ###

* Repo owner
Marco Lam : marcolam053@gmail.com